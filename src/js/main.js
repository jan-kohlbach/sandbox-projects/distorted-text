const {body} = document;
const text = body.querySelector('h1');
const offset = 30;

const moveShadow = (e) => {
  const width = body.offsetWidth;
  const height = body.offsetHeight;
  let mouseX = e.offsetX;
  let mouseY = e.offsetY;

  mouseX += e.target.offsetLeft;
  mouseY += e.target.offsetTop;

  const xOffset = Math.round(((mouseX / width) * offset) - (offset / 2));
  const yOffset = Math.round(((mouseY / height) * offset) - (offset / 2));

  text.style.textShadow = `
    ${xOffset}px ${yOffset}px 0 rgba(239, 71, 111, .7),
    ${xOffset * -1}px ${yOffset}px 0 rgba(255, 209, 102, .7),
    ${yOffset}px ${xOffset * -1}px 0 rgba(6, 214, 160, .7),
    ${yOffset * -1}px ${xOffset}px 0 rgba(17, 138, 178, .7)
  `;
};

body.addEventListener('mousemove', moveShadow);
